import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlists.interfaces';
import { PlaylistsService } from './playlists.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-playlists',
  template: `
  <div class="row">
    <div class="col">
      <app-items-list 
        [items]="playlists$ | async"
        (selectedChange)="select($event)"></app-items-list>
    </div>
    <div class="col">
      <router-outlet></router-outlet>
      <!-- <app-playlist-details
      (playlistChange)="save($event)"
      [playlist]="selected"></app-playlist-details> -->
    </div>
  </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selected: Playlist

  playlists$ = this.playlistsService.getPlaylists().pipe(
    tap(playlists => {
      if(!this.selected){
        setTimeout(()=>{
          // this.select(playlists[0])
        })
      }
    })
  )

  constructor(private playlistsService: PlaylistsService, private router:Router) { }

  save(playlist:Playlist){
    this.playlistsService.savePlaylist(playlist)
  }

  select(playlist: Playlist) {
    this.router.navigate(['playlists', playlist.id])
    // this.selected = playlist
  }

  ngOnInit() {
    // this.select(this.playlists[0])
  }

}
