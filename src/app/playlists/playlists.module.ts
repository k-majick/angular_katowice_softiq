import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms'

import { PlaylistsComponent } from './playlists.component';
import { ItemsListComponent } from './items-list.component';
import { ListItemComponent } from './list-item.component';
import { PlaylistDetailsComponent } from './playlist-details.component';

import { HighlightDirective } from './highlight.directive';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsService } from './playlists.service';
import { PlaylistContainerComponent } from './playlist-container.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PlaylistsRoutingModule
  ],
  declarations: [
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    PlaylistContainerComponent
  ],
  providers: [PlaylistsService]
})
export class PlaylistsModule { }
