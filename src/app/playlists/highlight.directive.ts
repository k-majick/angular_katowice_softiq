import { Directive, ElementRef, Input, OnInit, Attribute, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]',
  // host:{
  //   '(mouseenter)':'onEnter($event)',
  //   "[style.borderLeftColor]":"hover? item.color : 'inherit'"
  // }
})
export class HighlightDirective implements OnInit {

  @Input('highlight')
  set highlight(color){
    this.color = color
  }

  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.hover ? this.color : 'inherit'
  }

  color:string

  hover:boolean

  @HostListener('mouseenter',['$event.x'])
  onEnter(){
    this.hover = true
    // this.elem.nativeElement.style.color = this.color
  }

  @HostListener('mouseleave')
  onLeave(){
    this.hover = false
    // this.elem.nativeElement.style.color = 'inherit'
  }

  ngOnChanges(changes){
    // console.log(changes.highlight.currentValue)
  }

  ngOnInit(){
    // this.elem.nativeElement.style.color = this.color
  }

  constructor(
    // @Attribute('color') color, 
    private elem:ElementRef
  ) {
    // console.log('hello', this.color)
    // $(elem.nativeElement)
  }


}
