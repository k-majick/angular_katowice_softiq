import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';
import { By } from '@angular/platform-browser';
import { FormsModule, NgModel } from '@angular/forms';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      imports:[
        FormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render message', () => {
    const p = fixture.debugElement.query( By.css('p') )
    expect(p.nativeElement.textContent).toContain('Test');
  });

  it('should render updated message', () => {
    component.message = "Testing123"
    
    fixture.detectChanges()

    const p = fixture.debugElement.query( By.css('p') )
    expect(p.nativeElement.textContent).toContain("Testing123");
  });

  it('should render message in input', () => {
    component.message = "Testing123"
    
    fixture.detectChanges()
    fixture.whenRenderingDone().then(()=>{
      const input = fixture.debugElement.query( By.directive(NgModel) )
      
      expect(input.nativeElement.value).toContain("Testing123");
    })
  });

  it('should change message using input', () => {

    fixture.whenRenderingDone().then(()=>{

    const input = fixture.debugElement.query( By.directive(NgModel) )
    input.nativeElement.value = "Testing123"
    // input.nativeElement.dispatchEvent(new Event('input'))   
    input.triggerEventHandler('input',{target:input.nativeElement})
     
    fixture.detectChanges()
      expect(input.nativeElement.value).toContain("Testing123");
    })
  });

});
