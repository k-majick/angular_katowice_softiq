import { Component, OnInit, Output, Input } from '@angular/core';
import { FormGroup, AbstractControl, FormControl, FormArray, FormBuilder, Validators, ValidatorFn, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { filter, distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { PartialObserver } from 'rxjs/Observer';
import { MusicService } from './music.service';

@Component({
  selector: 'app-search-form',
  template: `
    <form class="form-group mb-3" [formGroup]="queryForm">
        <input type="text" class="form-control" placeholder="Search" formControlName="query">
        <ng-container *ngIf="queryForm.dirty || queryForm.touched">
          <div *ngIf="queryForm.get('query').hasError('required')">Field required</div>
          <div *ngIf="queryForm.get('query').getError('minlength') as LengthError">
            Minimum {{ LengthError['requiredLength']}} characters
          </div>
          <div *ngIf="queryForm.get('query').getError('censor') as BadWord">
            Cant use word "{{ BadWord }}"
          </div>
        </ng-container>
        <div *ngIf="queryForm.pending">Please wait ...</div>
    </form>
  `,
  styles: [`
    form .ng-touched.ng-invalid,
    form .ng-dirty.ng-invalid {
      border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Input('query')
  set query(query){
    this.queryForm.patchValue({
      query
    },{
      emitEvent:false
    })
  }

  @Output()
  queryChange:Observable<string>

  constructor(
    private builder: FormBuilder
  ) {

    const censor = (badword: string): ValidatorFn => (control: AbstractControl) => {
      const isError = control.value == badword

      return isError ? {
        'censor': badword
      } : null
    }

    const asyncCensor = (badword: string): AsyncValidatorFn => (control: AbstractControl) => {
      // return this.http.get().map( reponse => errors )
      return Observable.create((observer: PartialObserver<ValidationErrors | null>) => {

        setTimeout(() => {
          observer.next(censor(badword)(control))
          observer.complete()
        }, 2000)

      })
    }

    this.queryForm = builder.group({
      'query': builder.control('', [
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ], [
          asyncCensor('batman')
      ])
    })
    
    this.queryChange = this.queryForm
      .get('query')
      .valueChanges
      .pipe(
        debounceTime(400),
        filter(query => query.length >= 3),
        distinctUntilChanged(),
    )
  }

  ngOnInit() {
  }

}
