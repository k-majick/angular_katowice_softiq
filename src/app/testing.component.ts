import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testing',
  template: `
    <p>{{message}}</p>
    <input [(ngModel)]="message" >
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  message = "Test"

  constructor() { }

  ngOnInit() {
  }

}
