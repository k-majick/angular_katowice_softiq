import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Album } from './music.interface';
import { HttpClient } from '@angular/common/http'

export const API_URL = new InjectionToken('Token for api url')

// type Response<T> = Record<"albums"|"artists",{
//   items: T[] 
// }>
type ApiResponse<name extends string, ItemType> = {
  readonly [P in name]: {
    items: ItemType[]
  }
}

// import 'rxjs/Rx'
// import 'rxjs/add/operator/map'
import { map, catchError, switchMap, tap, startWith } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { of } from 'rxjs/observable/of';


@Injectable()
export class MusicService {

  queries$ = new BehaviorSubject<string>('alice')
  albums$ = new BehaviorSubject<Album[]>([])

  constructor(
    @Inject(API_URL) public api_url,
    private http: HttpClient
  ) { 
    console.log(this.albums$)

    this.queries$.pipe(
      switchMap(query => this.http
        .get<ApiResponse<'albums', Album>>(this.api_url, {
          params: {
            type: 'album',
            q: query
          }
        })
      ),
      map(response => response.albums.items),
      catchError(err => []),
      // tap(console.log),
    ).subscribe(this.albums$)
  }

  search(query: string) {
    this.queries$.next(query)
  }

  getAlbums() {
    return this.albums$.asObservable()
  }


}
