import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { PlaylistsComponent } from './playlists.component';
import { PlaylistsService } from './playlists.service';
import { ItemsListComponent } from './items-list.component';

class ServiceMock{

}

describe('PlaylistsComponent', () => {
  let component: PlaylistsComponent;
  let fixture: ComponentFixture<PlaylistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistsComponent ],
      schemas:[
        CUSTOM_ELEMENTS_SCHEMA
      ],
      providers:[
        {
          provide: PlaylistsService,
          useClass: ServiceMock
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
