import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }

  authorize() {

    const client_id = '7851c56a880240eb81dc1dbd62d97b93',
      response_type = 'token',
      redirect_uri = 'http://localhost:4200/'

    const url = `https://accounts.spotify.com/authorize`
      + `?client_id=${client_id}&response_type=${response_type}&redirect_uri=${redirect_uri}`

    sessionStorage.removeItem('token')
    window.location.replace(url)
  }


  private token = ''

  getToken() {

    this.token = JSON.parse(sessionStorage.getItem('token'))

    if (!this.token) {
      const match = window.location.hash.match(/access_token=([^&]*)/)
      this.token = match && match[1]

      sessionStorage.setItem('token', JSON.stringify(this.token))
      window.location.hash = ''
    }

    if (!this.token) {
      this.authorize()
    }

    return this.token
  }

}
