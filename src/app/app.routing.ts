import { Routes, RouterModule } from '@angular/router'
// import { PlaylistsComponent } from './playlists/playlists.component';
// import { MusicSearchComponent } from './music/music-search.component';

const routes:Routes = [
    { path: '', redirectTo: 'playlists', pathMatch:'full' },
    { path: 'music', loadChildren: './music/music.module#MusicModule' },
    { path: '**', redirectTo: 'playlists', pathMatch:'full' },
]

export const Routing = RouterModule.forRoot(routes,{
    // enableTracing:true,
    // useHash: true
})